module.exports = {
    configureWebpack:
    {
        devtool: 'source-map',
    },
    publicPath: '/admin/',
    outputDir: '../admin/',
};
