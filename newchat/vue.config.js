module.exports = {
    configureWebpack:
    {
        devtool: 'source-map',
    },
    publicPath: '/chat/',
    outputDir: '../chat/',
    filenameHashing: false,
};
