module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'indent': ['error', 4],
    'brace-style': ['error', 'allman'],
    'no-throw-literal': 0,
    'max-len': ['error', 120],
    'object-shorthand': ['error', 'always'],
    'curly': ["error", "all"],
    'quotes': ['error', 'single'],
    'no-restricted-syntax': [ 
        'error',
        { 
            selector: 'LabeledStatement', 
            message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.', 
        }, 
        { 
            selector: 'WithStatement', 
            message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.', 
        }, 
    ],
    'no-continue': 0,
    'no-console': 0,
    'no-param-reassign': ['error', { 'props': false }],
    'linebreak-style': [1, 'unix'],
  },
};
